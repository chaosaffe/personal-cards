# Personal Cards

### FontAwesome

https://fontawesome.com/

Icons from FontAwesome are indicated by `FA`, followed by the address and Style

### Google Noto Color Emoji

https://www.google.com/get/noto/#emoji-zsye-color

Icons from Google Noto Color Emoji are indicated by `NCE`, followed by the unicode address

### Font

Except where otherwise indicated Roboto Medium should be used.

### Orientation

Cards shall be oriented horizontally

## Front
```
Background Color: #ffffff
Background Image:
  Monkey Face (NCE: U+1F435),
  Grayscale,
  Color: #e0e4eb,
  Scaled for glyph height to match card height,
  Aligned left,
  Offset to cut off 20% of left side of glyph
Font Color: #596a87
Text:
  Jason Murray (Roboto Mono Bold)
  (FA: f1fa, Solid) jason@chaosaffe.io
  (FA: f3cd, Solid) +49 176 3241 7962
  (FA: f108, Solid) https://kube.life/
```
## Back
```
Background Color: #596a87
Background Image:
  See-No-Evil Monkey (NCE: U+1F648),
  Grayscale,
  Color: #e0e4eb,
  Scaled for glyph height to match card height,
  Aligned left,
  Offset to cut off 20% of left side of glyph
Font Color: #ffffff
Text:
  (FA: f099, Solid) twitter.com/chaosaffe
  (FA: f39e, Solid) facebook.com/chaosaffe
  (FA: f09b, Solid) github.com/chaosaffe
  (FA: f296, Solid) gitlab.com/chaosaffe
  (FA: f4f5, Solid) keybase.io/chaosaffe
  (https://en.divelogs.de/img/logo_top.png) divelogs.de/profile/chaosaffe
```